FROM openjdk:18

ARG JAR_FILE=build/libs/graphql-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} graphql.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","graphql.jar"]
