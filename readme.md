- Проект запускается на Docker.


- Для запуска проекта используем команду в Terminal `docker-compose up` мы должны находиться в той директории где лежит
  файл docker-compose

- endpoint для отправки запроса `http://localhost:8080/playground`

---

- mutation Создание Worker

```
mutation {
  createWorker(worker: {
          name: "Roma"
          money: 0
      }) {
          id
          name
          money
      }
}
```

---

- mutation Создание Task

```
mutation {
    createTask(task: {
        name: "Run"
        description: "Run Home"
        price: 100
        title: "Title"
    }) {
        name
    		description
    		price
    		title
    }
}
```

---

- mutation Взять задачу

```
mutation {
  takeTask(nameWorker: "Roma", nameTask: "Run")
}
```

---

- Обновить Task

```
mutation {
    updateTask(nameTask: "Run", title:"Run", description: "Run", price:100){
    name
    title
    description
    price
  }
}
```

---

- mutation Отменить задачу

```
mutation{
    cancelTask(nameTask: "Run", nameWorker:"Roma")
}
```

---

- mutation Завершить задачу

```
mutation {
    taskDone(nameTask: "Run", nameWorker: "Roma")
}
```

- mutation Удалить Task

```
mutation {
    deleteTask(name: "Run")
}
```

---

- mutation Удалить Worker

```
mutation {
    deleteWorker(name: "Roma")
}
```

---

- Query Найти задачу по имени

```
{
  findByNameTask(name:"Run") {
    id
    name
    description
    title
    price
    worker {
      name
    }
  }
}
```

---

- Query Найти Worker по имени

```
{
    findByNameWorker(name: "Roma") {
        id
        name
        money

    }
}
```

