package com.example.grapqhl.entity.enam;

public enum Status {
    Created,
    InProgress,
    Done,
    Cancellation
}
