package com.example.grapqhl.expationHedler;

import graphql.GraphQLException;
import graphql.kickstart.spring.error.ThrowableGraphQLError;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Component
public class ExceptionHandlerGraphQL {

    @ExceptionHandler(GraphQLException.class)
    public ThrowableGraphQLError graphQL(GraphQLException graphQLException) {
        return new ThrowableGraphQLError(graphQLException);
    }
}
