package com.example.grapqhl.grapqhl.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TaskInput {
    String name;
    String description;
    Integer price;
    String title;

}
