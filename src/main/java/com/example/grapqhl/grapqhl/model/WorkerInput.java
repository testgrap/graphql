package com.example.grapqhl.grapqhl.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class WorkerInput {
    String name;
    int money;
}
