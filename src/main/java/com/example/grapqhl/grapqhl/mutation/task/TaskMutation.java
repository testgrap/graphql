package com.example.grapqhl.grapqhl.mutation.task;

import com.example.grapqhl.entity.Task;
import com.example.grapqhl.entity.enam.Status;
import com.example.grapqhl.grapqhl.model.TaskInput;
import com.example.grapqhl.repository.TaskRepository;
import graphql.GraphQLException;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class TaskMutation implements GraphQLMutationResolver {

    private final TaskRepository taskRepository;

    @Transactional
    public Task createTask(TaskInput task) {
        boolean present = taskRepository.findByName(task.getName()).isPresent();
        if (present) {
            throw new GraphQLException("Задача с таким именем уже существует");
        }

        Task saveTask = Task.builder()
                .name(task.getName())
                .description(task.getDescription())
                .status(Status.Created)
                .price(task.getPrice())
                .title(task.getTitle())
                .build();

        return taskRepository.save(saveTask);
    }

    @Transactional
    public Task updateTask(String nameTask, String title, String description, int price) {
        Task task = taskRepository.findByName(nameTask)
                .orElseThrow(() -> new GraphQLException("Нет задачи с таким именем"));
        task.setTitle(title);
        task.setDescription(description);
        task.setPrice(price);
        taskRepository.save(task);
        return task;
    }

    @Transactional
    public Boolean deleteTask(String name) {
        Task task = taskRepository.findByName(name)
                .orElseThrow(() -> new GraphQLException("Нет задачи с таким именем"));

        if (task.getStatus() == Status.Done) {
            throw new GraphQLException("Задачу нельзя удалить, она уже выполнина");
        }
        if(task.getStatus() == Status.InProgress) {
            throw new GraphQLException("Задачу нельзя удалить, она находится в стадии выполнения");
        }

        taskRepository.deleteByName(name);
        return true;
    }
}
