package com.example.grapqhl.grapqhl.mutation.worker;

import com.example.grapqhl.entity.Task;
import com.example.grapqhl.entity.Worker;
import com.example.grapqhl.entity.enam.Status;
import com.example.grapqhl.grapqhl.model.WorkerInput;
import com.example.grapqhl.repository.TaskRepository;
import com.example.grapqhl.repository.WorkerRepository;
import graphql.GraphQLException;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class WorkerMutation implements GraphQLMutationResolver {

    private final WorkerRepository workerRepository;

    private final TaskRepository taskRepository;

    @Transactional
    public Worker createWorker(WorkerInput workerInput) {
        boolean present = workerRepository.findByName(workerInput.getName()).isPresent();
        if (present) {
            throw new GraphQLException("Worker с таким именем уже существует");
        }

        Worker build = Worker.builder()
                .name(workerInput.getName())
                .money(workerInput.getMoney())
                .build();

        return workerRepository.save(build);
    }

    @Transactional
    public Boolean deleteWorker(String name) {
        workerRepository.findByName(name)
                .orElseThrow(() -> new GraphQLException("Нет работника с таким именем"));

        workerRepository.deleteByName(name);
        return true;
    }

    @Transactional
    public Boolean cancelTask(String nameTask, String nameWorker) {
        Task task = taskRepository.findByName(nameTask)
                .orElseThrow(() -> new GraphQLException("Нет задачи с таким именем"));

        if (task.getWorker() == null) {
            throw new GraphQLException("Задачу нельзя отменить так как не назначен исполнитель");
        }

        if (task.getStatus() == Status.Done) {
            throw new GraphQLException("Нельзя отменить выполненную задачу");
        }

        Worker worker = workerRepository.findByName(nameWorker)
                .orElseThrow(() -> new GraphQLException("Нет работника с таким именем"));

        task.setWorker(null);
        task.setStatus(Status.Cancellation);
        worker.getTask().remove(task);
        worker.getTask().add(task);

        workerRepository.save(worker);
        return true;
    }

    @Transactional
    Boolean taskDone(String nameTask, String nameWorker) {
        Task task = taskRepository.findByName(nameTask)
                .orElseThrow(() -> new GraphQLException("Нет задачи с таким именем"));

        Optional.ofNullable(task.getWorker())
                .orElseThrow(() -> new GraphQLException("У задачи нет исполнителя"));

        Worker worker = workerRepository.findByName(nameWorker)
                .orElseThrow(() -> new GraphQLException("Нет работника с таким именем"));

        task.setStatus(Status.Done);
        worker.setMoney(worker.getMoney() + task.getPrice());

        workerRepository.save(worker);
        taskRepository.save(task);
        return true;
    }

    @Transactional
    public Boolean takeTask(String nameWorker, String nameTask) {
        Task task = taskRepository.findByName(nameTask)
                .orElseThrow(() -> new GraphQLException("Нет задачи с таким именем"));
        Worker worker = workerRepository.findByName(nameWorker)
                .orElseThrow(() -> new GraphQLException("Нет работника с таким именем"));

        if (task.getWorker() != null) {
            throw new GraphQLException("У задачи уже есть исполнитель");
        }

        task.setWorker(worker);
        task.setStatus(Status.InProgress);
        worker.getTask().add(task);

        workerRepository.save(worker);
        return true;
    }

}
