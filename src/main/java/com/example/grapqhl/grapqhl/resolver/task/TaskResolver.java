package com.example.grapqhl.grapqhl.resolver.task;

import com.example.grapqhl.entity.Task;
import com.example.grapqhl.repository.TaskRepository;
import graphql.GraphQLException;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class TaskResolver implements GraphQLQueryResolver {

    private final TaskRepository taskRepository;

    @Transactional(readOnly = true)
    public Task findByNameTask(String name) {
        return taskRepository.findByName(name)
                .orElseThrow(() -> new GraphQLException("Нет задачи с таким именем"));
    }
}
