package com.example.grapqhl.grapqhl.resolver.task.subQuery;

import com.example.grapqhl.entity.Task;
import com.example.grapqhl.entity.Worker;
import com.example.grapqhl.repository.WorkerRepository;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class SubQueryWorker implements GraphQLResolver<Task> {
    private final WorkerRepository workerRepository;

    public Worker worker(Task task) {
        return workerRepository.findByIdWorker(task.getId()).orElse(null);
    }
}
