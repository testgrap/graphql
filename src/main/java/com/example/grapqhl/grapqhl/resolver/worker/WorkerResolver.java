package com.example.grapqhl.grapqhl.resolver.worker;


import com.example.grapqhl.entity.Worker;
import com.example.grapqhl.repository.WorkerRepository;
import graphql.GraphQLException;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class WorkerResolver implements GraphQLQueryResolver {
    private final WorkerRepository userRepository;

    @Transactional(readOnly = true)
    public Worker findByNameWorker(String name) {
        return userRepository.findByName(name)
                .orElseThrow(() -> new GraphQLException("Нет задачи с таким именем"));
    }


}
