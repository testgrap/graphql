package com.example.grapqhl.grapqhl.resolver.worker.subQuery;

import com.example.grapqhl.entity.Task;
import com.example.grapqhl.entity.Worker;
import com.example.grapqhl.repository.TaskRepository;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SubQueryTask implements GraphQLResolver<Worker> {

    private final TaskRepository taskRepository;

    public List<Task> task(Worker worker) {
        return taskRepository.findByIdWorker(worker.getId());
    }

}
