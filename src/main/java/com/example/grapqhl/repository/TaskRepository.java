package com.example.grapqhl.repository;

import com.example.grapqhl.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("select t from Task t where t.worker.id = :id")
    List<Task> findByIdWorker(@Param(value = "id") Long id);

    Optional<Task> findByName(String name);

    void deleteByName(String name);

}
