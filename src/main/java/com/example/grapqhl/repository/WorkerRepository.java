package com.example.grapqhl.repository;

import com.example.grapqhl.entity.Worker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface WorkerRepository extends JpaRepository<Worker, Long> {
    Optional<Worker> findByName(String name);

    @Query("select w from Worker w join Task t on t.id = :id where t.worker is not null")
    Optional<Worker> findByIdWorker(@Param(value = "id") Long id);

    void deleteByName(String name);
}
