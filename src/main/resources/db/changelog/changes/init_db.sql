--liquibase formatted sql

--changeset test:1
create table if not exists worker
(
    id      serial primary key,
    name    varchar(32) not null unique,
    money   int
);

--changeset test:2
create table if not exists task
(
    id          serial primary key,
    name        varchar(32) not null unique,
    description varchar(124),
    status      varchar(24) not null,
    worker_id   int,
    price       int         not null,
    title       varchar(32)
);










