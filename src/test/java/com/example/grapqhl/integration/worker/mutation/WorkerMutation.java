package com.example.grapqhl.integration.worker.mutation;


import com.example.grapqhl.GraphQLApplicationTests;
import com.graphql.spring.boot.test.GraphQLResponse;
import com.graphql.spring.boot.test.GraphQLTestTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WorkerMutation extends GraphQLApplicationTests {

    public WorkerMutation(GraphQLTestTemplate graphQLTestTemplate) {
        super(graphQLTestTemplate);
    }

    @BeforeEach
    void init() throws IOException {
        graphQLTestTemplate.postForResource("graphQL/worker/beforeEachInit/worker.graphql");
        graphQLTestTemplate.postForResource("graphQL/worker/beforeEachInit/task.graphql");
    }

    @Test
    void createWorker() throws IOException {
        GraphQLResponse response =
                graphQLTestTemplate.postForResource("graphQL/worker/createWorker.graphql");

        response.getStatusCode().is2xxSuccessful();
        assertEquals("Dima", response.get("$.data.createWorker.name"));
        assertEquals("0", response.get("$.data.createWorker.money"));
    }

    @Test
    void deleteWorker() throws IOException {
        graphQLTestTemplate
                .postForResource("graphQL/worker/deleteWorker.graphql")
                .getStatusCode().is2xxSuccessful();

    }

    @Test
    void takeTask() throws IOException {
        graphQLTestTemplate
                .postForResource("graphQL/worker/takeTaskWorker.graphql")
                .getStatusCode().is2xxSuccessful();

    }

    @Test
    void cancelTask() throws IOException {
        graphQLTestTemplate.postForResource("graphQL/worker/takeTaskWorker.graphql");

        graphQLTestTemplate
                .postForResource("graphQL/worker/cancelTaskWorker.graphql")
                .getStatusCode().is2xxSuccessful();

    }

    @Test
    void taskDone() throws IOException {
        graphQLTestTemplate.postForResource("graphQL/worker/takeTaskWorker.graphql");
        graphQLTestTemplate
                .postForResource("graphQL/worker/taskDoneWorker.graphql")
                .getStatusCode().is2xxSuccessful();
    }

}